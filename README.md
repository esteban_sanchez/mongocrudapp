# Simple "Bear" API #
## Deployed: https://mongoose-crud-api.herokuapp.com/api/bears ##
## Routes: ##
### 
* GET '/api/bears' View all Bears
* POST '/api/bears' Create a new Bear
* GET '/api/bears/:bear_id' View specific Bear
* PUT '/api/bears/:bear_id' Update specific Bear
* DELETE '/api/bears/:bear_id' Delete specific Bear ###
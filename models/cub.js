var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CubSchema   = new Schema({
    name: String,
    age: Number,
    bear: [
    	{ type: Schema.Types.ObjectId, ref: 'Bear'}
    ]
});

module.exports = mongoose.model('Cub', CubSchema);
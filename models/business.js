var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var BusinessSchema   = new Schema({
    name: String,
});

module.exports = mongoose.model('Business', BusinessSchema);
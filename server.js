

var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var port 	= 	process.env.PORT || 3000;
var mongoose   = require('mongoose');
var Bear     = require('./models/bear');
var Cub      = require('./models/cub');
var Business      = require('./models/business');


mongoose.connect('mongodb://alexwang949:lagalaxy@jello.modulusmongo.net:27017/nah8uQux'); // connect to our database


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// get an instance of the express Router
var router = express.Router(); 

router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});



router.get('/', function(req, res) {
    res.json({ message: 'welcome to the bear api' });   
});


router.route('/bears')

    // create a bear (accessed at POST http://localhost:8080/api/bears)
    .post(function(req, res) {
        
        var bear = new Bear();      // create a new instance of the Bear model
        bear.name = req.body.name;  // set the bears name (comes from the request)
        bear.age = req.body.age;

        // save the bear and check for errors
        bear.save(function (err) {
            if (err) res.send(err);

            res.json({ message: 'Bear created!' });
        })

    })

    .get(function (req, res) {

        Bear.find(function(err, bears) {
            if (err) res.send(err);

            res.json(bears);
        })

    })

router.route('/bears/:bear_id')
    
    .get(function (req, res) {

        Bear.findById(req.params.bear_id, function (err, bear) {
            if (err) res.send(err);
            res.json(bear);
        })

    })

    .put(function (req, res) {

        Bear.findById(req.params.bear_id, function (err, bear) {

            if (err) res.send(err);

            bear.name = req.body.name; //update bear info

            bear.save(function (err) {

            if (err) res.send(err);

                res.json({ message: 'bear info updated!'})
            })
        })
    })

    .delete(function (req, res) {

        Bear.remove({
            _id: req.params.bear_id
        }, function (err, bear) {
            if (err) res.send(err);

            res.json({ message: 'bear deleted!'})
        })
    })

router.route('/bears/:bear_id/cubs')
    
    .post(function (req, res) {

        var cub = new Cub();

        cub.name = req.body.name;
        cub.age = req.body.age;
        cub.bear.push(req.params.bear_id);

        cub.save(function (err) {
            if (err) res.send(err);
            res.json(cub);
            // res.json({ message: 'cub created!' });
        })
    })

    .get(function (req, res) {

        Cub.find({ bear: req.params.bear_id 
        }, function (err, cub) {
            if (err) res.send(err);
            res.json(cub);
            
        })


    })

router.route('/cubs')

    .get(function (req, res) {

        Cub.find(function(err, cubs) {
            if (err) res.send(err);
            res.json(cubs);
        })

    });


        

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);



